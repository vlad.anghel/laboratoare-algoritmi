package utils

import "strings"
import "fmt"

func Title(s string) {
	l := len(s)
	l += 2
	padding_size := (80 - l) / 2
	padding := strings.Repeat("=", padding_size)
	fmt.Print("\n", padding, " ", s, " ", padding, "\n\n")
}
