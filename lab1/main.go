package main

import (
	"fmt"
	"math/rand"
	"strings"
)

func title(s string) {
	l := len(s)
	l += 2
	padding_size := (80 - l)/2
	padding := strings.Repeat("=", padding_size)
	fmt.Print("\n", padding," ", s, " ", padding, "\n\n")
}

func main() {
	title("INSERTION SORT")
	a := make([]int, 0)
	for i:= 0; i != 10; i++ {
		a = append(a, rand.Int() % 100)
	}

	fmt.Printf("%#v\n", a)

	insertion(a)

	fmt.Printf("%#v\n", a)

	title("SELECTION SORT")
	a = make([]int, 0)
	for i:= 0; i != 10; i++ {
		a = append(a, rand.Int() % 100)
	}

	fmt.Printf("%#v\n", a)

	selection_sort(a)

	fmt.Printf("%#v\n", a)

	title("EUCLID CMMDC")

	cmmdc_a := rand.Int() % 100
	cmmdc_b := rand.Int() % 100

	fmt.Printf("cmmdc(%d, %d) = %d\n", cmmdc_a, cmmdc_b, cmmdc(cmmdc_a, cmmdc_b))

	title("FIBO_R")
	n := rand.Int() % 10
	fibr_arg := rand.Int() % 10
	fibr_arg = n
	fmt.Printf("fibo_r(%d) = %d\n", fibr_arg, fibo_r(fibr_arg))

	title("FIBO_I")
	fibi_arg := rand.Int() % 10
	fibi_arg = n
	fmt.Printf("fibo_r(%d) = %d\n", fibi_arg, fibo_r(fibi_arg))

	title("PRODUS MATRICI")
	m1 := matrice(3, 2)
	m2 := matrice(2, 4)
	m3 := produs(m1, m2)
	fmt.Println("m1 = ")
	print_matrix(m1)
	fmt.Println("m2 = ")
	print_matrix(m2)
	fmt.Println("m3 = m1 * m2")
	print_matrix(m3)

	russe_a := rand.Int() % 20
	russe_b := rand.Int() % 20
	title("RUSSE")
	fmt.Printf("russe(%d, %d) = %d\n",russe_a, russe_b, russe(russe_a, russe_b))
}

