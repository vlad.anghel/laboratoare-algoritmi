package main

func insertion(a []int) {
	for j := 0; j != len(a); j++ {
		key := a[j]

		i := j - 1

		for i >= 0 && a[i] > key {
			a[i + 1] = a[i]
			i--
		}
		a[i+1] = key
	}
}

