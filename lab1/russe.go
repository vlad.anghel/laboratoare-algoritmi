package main

func russe(a int, b int) int {
	x := [256]int{}
	y := [256]int{}

	x[0] = a
	y[0] = b
	i := 0
	for x[i] > 1 {
		x[i + 1] = x[i]/2
		y[i + 1] = y[i] + y[i]
		i++
	}

	prod := 0
	for i > 0 {
		if (x[i]) % 2 == 1 {
			prod += y[i]
		}
		i--
	}
	return prod
}

