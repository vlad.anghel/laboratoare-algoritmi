package main

import (
	"fmt"
	"math/rand"
)

func matrice(n int, m int) [][]int {
	matrix := make([][]int, n)
	for i := 0; i != n; i++ {
		matrix[i] = make([]int, m)
		for j := 0; j != m; j++ {
			matrix[i][j] = rand.Int() % 10
		}
	}
	return matrix
}

func print_matrix(m [][]int) {
	for _, v := range(m) {
		fmt.Print("\n\t")
		for _, k := range(v) {
			fmt.Print(k, "\t")
		}
		fmt.Println()
	}
}


func produs(a [][]int, b [][]int) [][]int {
	m := len(a)
	n := len(a[0])

	if n != len(b) {
		panic("matrici de dimensiuni invalide")
	}

	p := len(b[0])

	c := make([][]int, m)
	for i := 0; i != m; i++ {
		c[i] = make([]int, p)
	}

	for i := 0; i != m; i++ {
		for j := 0; j != p; j++ {
			for k := 0; k != n; k++ {
				c[i][j] = c[i][j] + a[i][k] * b[k][j]
			}
		}
	}
	return c
}
