package main

func selection_sort(a []int) {
	for i := 0; i < (len(a) - 1); i++ {
		minj := i
		minx := a[i]

		for j := i + 1; j < len(a); j++ {
			if a[j] < minx {
				minj = j
				minx = a[j]
			}
		}

		a[minj] = a[i]
		a[i] = minx
	}
}

