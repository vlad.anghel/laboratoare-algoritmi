package main

func cmmdc(n int, m int) int {

	for n != 0 {
		tmp := n
		n = m%n
		m = tmp
	}

	return m
}

