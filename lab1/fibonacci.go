package main

func fibo_r(n int) int {
	if n < 2 {
		return n
	}
	return fibo_r(n-1) + fibo_r(n-2)
}

func fibo_i(n int) int {
	i := 0
	j := 1
	s := 1
	for k := 1; k != n; k++ {
		i = j
		j = s
		s = i + j
	}

	return i
}
