package main

import (
	"fmt"
	"strings"
)

type DList struct {
	node *DNode
}

type DNode struct {
	next *DNode
	prev *DNode
	data int
}

func NewDList() DList {
	return DList{node: nil}
}

func (l DList) String() string {
	b := strings.Builder{}
	node := l.node
	first := (*DNode)(nil)
	for node != nil && node != first {
		if first == nil {
			first = node
		}

		b.WriteString(fmt.Sprint(node.data, " "))
		node = node.next
	}
	return b.String()
}

func (l *DList) Len() int {
	count := 0
	node := l.node
	first := (*DNode)(nil)
	for node != nil && first != node {
		if first == nil {
			first = node
		}
		count++
		node = node.next
	}
	return count
}

func (l *DNode) Insert(x int) {
	node := new(DNode)
	node.next = l.next
	node.data = x
	l.next = node
	node.prev = l
}

func (l *DList) InsertBack(x int) {
	if l.node == nil {
		l.node = new(DNode)
		l.node.data = x
		l.node.next = nil
		return
	}
	last := l.DNodeAt(99)
	last.next = new(DNode)
	last.next.next = nil
	last.next.data = x
	last.next.prev = last
}

func (l *DNode) UnlinkNext() {
	node := l.next
	if node == nil {
		return
	}
	l.next = node.next
	if l.next != nil {
		l.next.prev = l
	}
}

func (l *DList) RemoveFirst() {
	if l.node == nil {
		return
	}
	l.node = l.node.next
	l.node.prev = nil

}

func (l *DList) DNodeAt(index int) *DNode {
	node := l.node
	last := l.node
	for index != 0 && node != nil {
		index--
		last = node
		node = node.next
	}
	return last
}

func (l *DNode) Next() *DNode {
	return l.next
}
