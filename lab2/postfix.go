package main

import "unicode"

func isMathOp(r rune) bool {
	return r == '+' || r == '-' || r == '*'
}

func postfix(expr string) int {
	stack := NewStack(20)
	for _, r := range expr {
		if unicode.IsDigit(r) {
			stack.push(int(r - '0'))
		} else if isMathOp(r) {
			x := stack.pop()
			y := stack.pop()

			result := 0

			switch r {
			case '+':
				result = x + y
			case '-':
				result = y - x
			case '*':
				result = x * y
			}

			stack.push(result)
		}
	}

	return stack.pop()
}
