package main

type Queue struct {
	data []int
	head int
	tail int
}

func NewQueue(max int) Queue {
	return Queue{data: make([]int, max), head: 0, tail: 0}
}

func (q *Queue) enqueue(x int) {
	q.data[q.tail] = x
	if q.tail == len(q.data) {
		q.tail = 1
	} else {
		q.tail++
	}
}

func (q *Queue) pop() int {
	x := q.data[q.head]
	if q.head == len(q.data) {
		q.head = 1
	} else {
		q.head++
	}
	return x
}


