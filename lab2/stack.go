package main

type Stack struct {
	data []int
	top  int
}

func (s *Stack) push(x int) {
	s.data[s.top] = x
	s.top++
}

func (s *Stack) empty() bool {
	return s.top == 0
}

func (s *Stack) pop() int {
	if s.empty() {
		panic("stack underflow")
	}
	s.top--
	x := s.data[s.top]
	return x
}

func NewStack(max int) Stack {
	return Stack{data: make([]int, max), top: 0}
}


type TStack[T any] struct {
	data []T
	top  int
}

func (s *TStack[T]) push(x T) {
	s.data[s.top] = x
	s.top++
}

func (s *TStack[T]) empty() bool {
	return s.top == 0
}

func (s *TStack[T]) len() int {
	return s.top
}

func (s *TStack[T]) pop() T {
	if s.empty() {
		panic("stack underflow")
	}
	s.top--
	x := s.data[s.top]
	return x
}

func NewTStack[T any](max int) TStack[T] {
	return TStack[T]{data: make([]T, max), top: 0}
}

