package main

import (
	"fmt"
	"strings"
)

type List struct {
	node *Node
}

type Node struct {
	next *Node
	data int
}

func NewList() List {
	return List{}
}

func (l List) String() string {
	b := strings.Builder{}
	node := l.node
	for node != nil {
		b.WriteString(fmt.Sprint(node.data, " "))
		node = node.next
	}
	return b.String()
}

func (l *List) Len() int {
	count := 0
	node := l.node
	for node != nil {
		count++
		node = node.next
	}
	return count
}

func (l *Node) Insert(x int) {
	node := new(Node)
	node.next = l.next
	node.data = x
	l.next = node
}

func (l *List) InsertBack(x int) {
	last := l.NodeAt(99)
	if last == nil {
		l.node = new(Node)
		l.node.data = x
		l.node.next = nil
		return
	}
	node := new(Node)
	node.next = nil
	node.data = x
	last.next = node
}

func (l *Node) UnlinkNext() {
	node := l.next
	l.next = node.next
}

func (l *List) RemoveFirst() {
	if l.node == nil {
		return
	}
	l.node = l.node.next
}

func (l *List) NodeAt(index int) *Node {
	node := l.node
	last := l.node
	for index != 0 && node != nil {
		index--
		last = node
		node = node.next
	}
	return last
}

func (l *Node) Next() *Node {
	return l.next
}
