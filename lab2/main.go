package main

import (
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/vlad.anghel/laboratoare-algoritmi/utils"
)

type Direction int

const Up Direction = 1
const Down Direction = 2
const Left Direction = 3
const Right Direction = 4

func (d Direction) String() string {
	switch d {
	case Up:
		return "Up"
	case Down:
		return "Down"
	case Left:
		return "Left"
	case Right:
		return "Right"
	default:
		return "unknown"
	}
}

func (d Direction) Translate(x, y int) (int, int) {
	switch d {
	case Up:
		return x, y - 1
	case Down:
		return x, y + 1
	case Left:
		return x - 1, y
	case Right:
		return x + 1, y
	default:
		panic("don't know how to translate")
	}
}

func (d Direction) XY(x, y int) [2]int {
	new_x, new_y := d.Translate(x, y)
	return [2]int{new_x, new_y}
}

type Maze struct {
	lines []string
}

func LoadMaze(path string) (m Maze, err error) {
	data, err := ioutil.ReadFile("in.txt")
	if err != nil {
		return
	}

	lines := strings.Split(string(data), "\n")

	m = Maze{lines: lines[:len(lines)-1]}
	return
}

func (m *Maze) FindSym(r rune) (int, int) {
	for i := range m.lines {
		for j, k := range m.lines[i] {
			if k == r {
				return j, i
			}
		}
	}

	panic("Maze doesn't have a " + string(r))
}

func (m *Maze) FindStart() (int, int) {
	return m.FindSym('S')
}

func (m *Maze) FindEnd() (int, int) {
	return m.FindSym('E')
}

func (m *Maze) MovesAt(x int, y int) []Direction {
	if x < 0 || y < 0 || x >= len(m.lines[0]) || y >= len(m.lines) {
		return nil
	}
	if m.lines[y][x] == '#' {
		return nil
	}

	moves := make([]Direction, 0)
	if x > 0 && m.lines[y][x-1] != '#' {
		moves = append(moves, Left)
	}
	if x < (len(m.lines[0]) - 1) && m.lines[y][x+1] != '#' {
		moves = append(moves, Right)
	}
	if y > 0 && m.lines[y-1][x] != '#' {
		moves = append(moves, Up)
	}
	if y < (len(m.lines)-1) && m.lines[y+1][x] != '#' {
		moves = append(moves, Down)
	}

	return moves
}

func (m *Maze) Solve() {
	visited := make(map[[2]int]bool, 0)
	path := NewTStack[[2]int](64)

	x, y := m.FindStart()
	fmt.Printf("Starting from %d %d\n", x, y)
	path.push([2]int{x, y})
	visited[[2]int{x, y}] = true

	fin_x, fin_y := m.FindEnd()

	for {
		moves := m.MovesAt(x, y)

		var next Direction
		ok := false

		fmt.Printf("Checking %v for %d %d\n", moves, x, y)
		fmt.Printf("Visited: %v\n", visited)
		ok = false
		for _, d := range moves {
			nx, ny := d.Translate(x, y)
			fmt.Printf("In direction %s is %d %d which has been visited: %t\n", d, nx, ny, visited[d.XY(x, y)])
			if !visited[d.XY(x, y)] {
				next = d
				ok = true
				break
			}
		}

		if !ok {

			if !path.empty() {
				a := path.pop()

				x = a[0]
				y = a[1]

				fmt.Printf("Went back to %d %d\n", x, y)
				visited[[2]int{x, y}] = true

				m.Display(&path)
			} else {
				fmt.Println("STUCK")
			}
		} else {
			visited[next.XY(x, y)] = true
			path.push([2]int{x, y})
			x, y = next.Translate(x, y)
			fmt.Printf("Moving to %d %d\n", x, y)
			fmt.Printf("Path is: %v\n", path.data)
			m.Display(&path)
		}

		if x == fin_x && y == fin_y {
			utils.Title("final solution to the maze")
			path.push([2]int{x, y})
			m.Display(&path)
			return
		}

	}
}

func (m *Maze) Display(path *TStack[[2]int]) {
	for i, line := range m.lines {
		for j, block := range line {
			ch := block
			for k, v := range path.data {
				if k == path.len() {
					break
				}
				if v[0] == j && v[1] == i {
					ch = '*'
				}
			}
			fmt.Printf("%c", ch)
		}
		fmt.Println("")
	}
}

func main() {

	utils.Title("QUEUE/COADA")

	q := NewQueue(20)

	q.enqueue(2)
	q.enqueue(3)
	q.enqueue(4)

	for i := 0; i != 3; i++ {
		fmt.Printf("%d %d\n", i, q.pop())
	}

	utils.Title("STACK/STIVA")

	s := NewStack(20)

	s.push(2)
	s.push(3)
	s.push(4)
	for i := 0; i != 3; i++ {
		fmt.Printf("%d %d\n", i, s.pop())
	}

	utils.Title("POSTFIX")

	expr := "512 + 4 * + 3 -"

	fmt.Printf("%s = %d\n", expr, postfix(expr))

	utils.Title("MAZE")

	maze, err := LoadMaze("in.txt")
	if err != nil {
		panic(err)
	}

	fmt.Printf("%#v\n", maze.lines)

	for x := 0; x < len(maze.lines[0]); x++ {
		for y := 0; y < len(maze.lines); y++ {
			fmt.Printf("[%d %d] ", x, y)
			moves := maze.MovesAt(x, y)
			for _, k := range moves {
				fmt.Printf("%s ", k)
			}
			fmt.Print("\t")
		}
		fmt.Println()
	}

	fmt.Printf("%v", maze.MovesAt(maze.FindStart()))

	utils.Title("MAZE SOLVE")

	maze.Solve()

	utils.Title("LINKED LIST")

	list := NewList()
	for i := 0; i != 5; i++ {
		fmt.Printf("Inserting %d\n", i)
		list.InsertBack(i)
	}

	node := list.node
	for node != nil {
		fmt.Printf("Next of %d is %p\n", node.data, node.Next())
		node = node.Next()
	}

	fmt.Println(list.String())

	fmt.Printf("list: %s\n", list)

	list.NodeAt(1).UnlinkNext()

	fmt.Printf("list: %s\n", list)

	list.NodeAt(list.Len()/2).Insert(42)

	fmt.Printf("list: %v\n", list)

	list.RemoveFirst()
	list.NodeAt(list.Len()-1).UnlinkNext()
	fmt.Printf("list: %v\n", list)

	utils.Title("DOUBLE LINKED LIST")

	dlist := NewDList()
	for i := 0; i != 5; i++ {
		fmt.Printf("Inserting %d\n", i)
		dlist.InsertBack(i)
	}

	dnode := dlist.node
	for dnode != nil {
		fmt.Printf("Next of %d is %p\n", dnode.data, dnode.Next())
		dnode = dnode.Next()
	}

	fmt.Println(dlist.String())

	fmt.Printf("dlist: %s\n", dlist)

	dlist.DNodeAt(1).UnlinkNext()

	fmt.Printf("dlist: %s\n", dlist)

	dlist.DNodeAt(dlist.Len()/2).Insert(42)

	fmt.Printf("dlist: %v\n", dlist)

	dlist.RemoveFirst()
	dlist.DNodeAt(dlist.Len()-1).UnlinkNext()
	fmt.Printf("dlist: %v\n", dlist)

	utils.Title("CIRCULAR DOUBLE LINKED LIST")
	dlist = NewDList()
	for i := 0; i != 5; i++ {
		fmt.Printf("Inserting %d\n", i)
		dlist.InsertBack(i)
		fmt.Printf("dlist.DNodeAt(dlist.Len() - 1).data: %v\n", dlist.DNodeAt(dlist.Len()).data)
	}

	dlist.node.prev = dlist.DNodeAt(dlist.Len())
	dlist.node.prev.next = dlist.node

	dnode = dlist.node
	first := (*DNode)(nil)
	for dnode != nil && dnode != first {
		if first == nil {
			first = dnode
		}
		fmt.Printf("Next of %d is %p\n", dnode.data, dnode.Next())
		dnode = dnode.Next()
	}

	fmt.Println(dlist.String())

	i := dlist.Len()
	fmt.Printf("i: %v\n", i)

	fmt.Printf("dlist.Len(): %v\n", dlist.Len())
	fmt.Printf("dlist: %s\n", dlist)

	dlist.DNodeAt(1).UnlinkNext()

	fmt.Printf("dlist.Len(): %v\n", dlist.Len())
	fmt.Printf("dlist: %s\n", dlist)

	dlist.DNodeAt(2).Insert(42)

	fmt.Printf("dlist: %s\n", dlist)

	dlist.RemoveFirst()
	dlist.DNodeAt(33).UnlinkNext()
	fmt.Printf("dlist: %s\n", dlist)

}
